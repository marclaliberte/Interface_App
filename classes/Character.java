public class Character {

	private string name;
	private string race;
	private string char_class;
	private string alignment;
	private int level;
	private int xp_pts;
	private int strength;
	private int dexterity;
	private int constitution;
	private int intelligence;
	private int wisdom;
	private int charisma;
	private int[] skills;
	private ArrayList<String> backpack;
	private ArrayList<String> additionnal_notes;
	private ArrayList<Throw> throwShortCuts;
	private int hp;
	private int action_pt;
	private int armor_class;
	private int[] defenses;
	private int speed;

	public string getName() {
		return this.name;
	}

	public void setName(string name) {
		this.name = name;
	}

	public string getRace() {
		return this.race;
	}

	public void setRace(string race) {
		this.race = race;
	}

	public string getChar_class() {
		return this.char_class;
	}

	public void setChar_class(string char_class) {
		this.char_class = char_class;
	}

	public string getAlignment() {
		return this.alignment;
	}

	public void setAlignment(string alignment) {
		this.alignment = alignment;
	}

	public int getLevel() {
		return this.level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getXp_pts() {
		return this.xp_pts;
	}

	public void setXp_pts(int xp_pts) {
		this.xp_pts = xp_pts;
	}

	public int getStrength() {
		return this.strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getDexterity() {
		return this.dexterity;
	}

	public void setDexterity(int dexterity) {
		this.dexterity = dexterity;
	}

	public int getConstitution() {
		return this.constitution;
	}

	public void setConstitution(int constitution) {
		this.constitution = constitution;
	}

	public int getIntelligence() {
		return this.intelligence;
	}

	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}

	public int getWisdom() {
		return this.wisdom;
	}

	public void setWisdom(int wisdom) {
		this.wisdom = wisdom;
	}

	public int getCharisma() {
		return this.charisma;
	}

	public void setCharisma(int charisma) {
		this.charisma = charisma;
	}

	public int getSkills() {
		return this.skills;
	}

	public void setSkills(int skills) {
		this.skills = skills;
	}

	public ArrayList<String> getAdditionnal_notes() {
		return this.additionnal_notes;
	}

	public void setAdditionnal_notes(ArrayList<String> additionnal_notes) {
		this.additionnal_notes = additionnal_notes;
	}

	public ArrayList<Throw> getThrowShortCuts() {
		return this.throwShortCuts;
	}

	public void setThrowShortCuts(ArrayList<Throw> throwShortCuts) {
		this.throwShortCuts = throwShortCuts;
	}

	public int getHp() {
		return this.hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAction_pt() {
		return this.action_pt;
	}

	public void setAction_pt(int action_pt) {
		this.action_pt = action_pt;
	}

	public int getArmor_class() {
		return this.armor_class;
	}

	public void setArmor_class(int armor_class) {
		this.armor_class = armor_class;
	}

	public int getDefenses() {
		return this.defenses;
	}

	public void setDefenses(int defenses) {
		this.defenses = defenses;
	}

	public int getSpeed() {
		return this.speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getModif() {
		// TODO - implement Character.getModif
		throw new UnsupportedOperationException();
	}

}