#ifndef CHARACTER_H
#define CHARACTER_H

class Character {

private:
	string name;
	string race;
	string char_class;
	string alignment;
	int level;
	int xp_pts;
	int strength;
	int dexterity;
	int constitution;
	int intelligence;
	int wisdom;
	int charisma;
	int skills;
	ArrayList<String> backpack;
	ArrayList<String> additionnal_notes;
	ArrayList<Throw> throwShortCuts;
	int hp;
	int action_pt;
	int armor_class;
	int defenses;
	int speed;

public:
	int getModif();
};

#endif
