
public class Throw {
    private int numbers;
    private int faces;
    private String name;

    public Throw(String s, int n, int f) {
        name = s;
        faces =f;
        numbers = n;
    }

    public int launch() {
        int result=0;
        for(int i=0;i<numbers;i++) {
            System.out.println(result);
            result+=Math.floor(Math.random()*faces)+1;
        }
        return result;
    }
    public int getFaces() {
        return faces;
    }
    public int getNumbers() {
        return numbers;
    }
    public String getName() {
        return name;
    }
}
