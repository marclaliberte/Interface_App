package com.example.propritaire.rpghelper.assets;


import java.io.Serializable;

public class Throw implements Serializable{
    private int numbers;
    private int faces;
    private String name;
    private int bonus;
    private Result res;

    //classe anonyme pour que le dé puisse retourner le total+tous les lancés
    public class Result implements Serializable{
        private int total;
        private String allThrows="";

        public void addToThrow(String a){
            allThrows +=a+" ";
        }

        public void setTotal(int a){
            total =a;
        }
    }

    //constructeur
    public Throw(String s, int n, int f, int b)
    {
        name = s;
        faces =f;
        numbers = n;
        bonus=b;
    }

    //set res
    public void launch() {
        int x,result=0;

        //delete res existant
        this.res=null;

        Result  r= new Result();
        for(int i=0;i<numbers;i++)
        {
            //System.out.println(result);
            x=(int)Math.floor(Math.random()*faces)+1;
            r.addToThrow(x+"");
            result+=x;
        }
        this.res=r;
        this.res.setTotal(result);
    }

    public int getFaces() {
        return faces;
    }
    public int getNumbers() {
        return numbers;
    }
    public String getName() {
        return name;
    }
    public int getBonus(){return bonus;}
    public String getAllThrows(){
        return res.allThrows;
    }
    public int getTotal(){
        return res.total+bonus;
    }
}
