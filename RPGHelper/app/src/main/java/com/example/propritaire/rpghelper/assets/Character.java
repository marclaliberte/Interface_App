package com.example.propritaire.rpghelper.assets;
import java.util.*;
import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Character implements Serializable{
	private String name;
	private String race;
	private String char_class;
	private String alignment;
	private int level;
	private int xp_pts;
	private int strength;
	private int dexterity;
	private int constitution;
	private int intelligence;
	private int wisdom;
	private int charisma;
	private ArrayList<Throw> rolls;
	private int hp;
	private int maxHp;
	private int action_pt;

	public Character(String name, String race, String charClass, String align, int str, int dex, int con, int intel, int wis, int charis, int life, int lvl, int xp)
    {
	    this.name=name;
	    this.race=race;
	    char_class=charClass;
	    alignment=align;
	    strength=str;
	    dexterity=dex;
	    constitution=con;
	    intelligence=intel;
	    wisdom=wis;
	    charisma=charis;
	    level=lvl;
	    xp_pts=xp;
	    rolls= new ArrayList<Throw>();
	    hp=life;
	    maxHp=life;
		rolls.add(new Throw("Initiative", 1, 10, 0));
		rolls.add(new Throw("To Hit", 1, 20, 0));
    }

	//This constructor is useful for when the character is edited
	public Character(String name, String race, String charClass, String align, int str, int dex, int con, int intel, int wis, int charis, int hp, int lvl, int xp, int maxHp, ArrayList<Throw> rolls)
	{
		this.name=name;
		this.race=race;
		char_class=charClass;
		alignment=align;
		strength=str;
		dexterity=dex;
		constitution=con;
		intelligence=intel;
		wisdom=wis;
		charisma=charis;
		level=lvl;
		xp_pts=xp;
		this.rolls= rolls;
		this.hp=hp;
		this.maxHp=maxHp;
	}


	public String getName() {
		return this.name;
	}
	public String getBasicInfo(){
		String result="Class: "+getChar_class()+"\n";
		result+="Aligment: "+getAlignment()+"\n";
		result+="Level: "+getLevel()+"\n";
		return result;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRace() {
		return this.race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getChar_class() {
		return this.char_class;
	}

	public void setChar_class(String char_class) {
		this.char_class = char_class;
	}

	public String getAlignment() {
		return this.alignment;
	}

	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}

	public int getLevel() {
		return this.level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getXp_pts() {
		return this.xp_pts;
	}

	public void setXp_pts(int xp_pts) {
		this.xp_pts = xp_pts;
	}

	public int getStrength() {
		return this.strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getDexterity() {
		return this.dexterity;
	}

	public void setDexterity(int dexterity) {
		this.dexterity = dexterity;
	}

	public int getConstitution() {
		return this.constitution;
	}

	public void setConstitution(int constitution) {
		this.constitution = constitution;
	}

	public int getIntelligence() {
		return this.intelligence;
	}

	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}

	public int getWisdom() {
		return this.wisdom;
	}

	public void setWisdom(int wisdom) {
		this.wisdom = wisdom;
	}

	public int getCharisma() {
		return this.charisma;
	}

	public void setCharisma(int charisma) {
		this.charisma = charisma;
	}

	public ArrayList<Throw> getRollsList(){
		return rolls;
	}

	public ArrayList<Throw> getRolls() {
		return this.rolls;
	}

	public ArrayList<String> getAllRollsName(){
		ArrayList<String> a= new ArrayList<String>();
		for(int i=0;i<this.getRolls().size();i++){
			a.add(this.getRolls().get(i).getName()+" "+this.getRolls().get(i).getNumbers()+"D"+this.getRolls().get(i).getFaces()+"+"+this.getRolls().get(i).getBonus());
		}
		return a;
	}
	public void enleverRoll(String name){
		for(int i=0;i<rolls.size();i++){
			if(rolls.get(i).getName().equals(name)){
				rolls.remove(rolls.get(i));
			}
		}
	}

	public String getCharDesc(){
		return char_class + " " + name + " level " + level;
	}

	public void setRolls(ArrayList<Throw> newRolls) {
		this.rolls = newRolls;
	}

	public int getMaxHp(){
		return maxHp;
	}

	public void setMaxHp(int maxHp){
		this.maxHp = maxHp;
	}

	public int getHp() {
		if(this.hp<0){
			this.hp=0;
			return 0;
		}
		if(this.hp>maxHp)
		{
			this.hp = maxHp;
			return maxHp;
		}
		return this.hp;
	}

	public void setHp(int hp) {
		this.hp += hp;
	}

	public int getAction_pt() {
		return this.action_pt;
	}

	public void setAction_pt(int action_pt) {
		this.action_pt = action_pt;
	}

	public int getThrowIndex(String name){
		for(int i=0;i<rolls.size();i++){
			if(rolls.get(i).getName().equals(name)){
				return i;
			}
		}
		return 0;
	}

	@Override
	public boolean equals(Object other){
		if (other == null || !(other instanceof Character)) {
			return false;
		}
		if (other == this){
			return true;
		}
		Character otherChar = (Character)other;

		return otherChar.getName().equals(name) && otherChar.getRace().equals(race) && otherChar.getChar_class().equals(char_class)
				&& otherChar.getAlignment().equals(alignment) && otherChar.getLevel() == level;
	}

}