package com.example.propritaire.rpghelper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.AlignmentSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.propritaire.rpghelper.assets.Character;
import com.example.propritaire.rpghelper.assets.SaveManager;
import com.example.propritaire.rpghelper.assets.Throw;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainScreen extends AppCompatActivity {
    Character mainChar;
    Button createRoll;
    ListView rollList;
    Button retBtn;
    Button damage;
    Button healing;
    EditText adjustLife;
    TextView life;

    //sounds related
    MediaPlayer mpsmall;
    MediaPlayer mpmid;
    MediaPlayer mpbig;
    Integer position;
    private DrawerLayout mDrawerLayout;
    private ArrayList<String> data = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        setCharacter();

        //init drawer layout
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.hearth);
        mDrawerLayout=findViewById(R.id.drawer_layout);

        //change color of nav bars
        getWindow().setNavigationBarColor(getColor(R.color.black));
        //change top title
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#FFFFFF'>"+mainChar.getName()+"</font>"));

        rollList=findViewById(R.id.rollList);
        rollList.setAdapter(new MyListAdapter(this, R.layout.roll_list_item, mainChar.getAllRollsName()));
        rollList.setFocusable(false);
        generateListContent();
        TextView basicInfo=findViewById(R.id.basicInfoDisplay);
        basicInfo.setText(mainChar.getBasicInfo());
        basicInfo.setTypeface(null, Typeface.BOLD);

        initMediaPlayer();
        initStats();
        initLifeAdjust();

        createRoll=findViewById(R.id.newRollBtn);
        createRoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpmid.start();
                Intent i = new Intent(MainScreen.this, CreateEditThrowActivity.class);
                i.putExtra("mainChar", mainChar);
                startActivity(i);

            }
        });
        /*
        retBtn=findViewById(R.id.retBtn);
        retBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpmid.start();
                SaveManager.saveCharacters(getApplicationContext());
                Intent i = new Intent(MainScreen.this, TitleScreen.class);
                startActivity(i);
            }
        });
        */
        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        //nav drawer related
                        switch (menuItem.getItemId()) {
                            case R.id.editCharNav:
                                Intent x = new Intent(MainScreen.this, CharacterActivity.class);
                                x.putExtra("mainChar", mainChar);
                                startActivity(x);
                                return true;
                            case R.id.deleteCharNav:
                                deleteCharacter();
                                return true;
                            case R.id.returnTitleScreen:
                                mpmid.start();
                                saveCharacterChanges();
                                Intent i = new Intent(MainScreen.this, TitleScreen.class);
                                startActivity(i);
                                return true;
                        }
                        return true;
                    }
                });
    }

    private void deleteCharacter()
    {
        //Popup pour valider la tentative d'effacement.
        //https://stackoverflow.com/questions/36747369/how-to-show-a-pop-up-in-android-studio-to-confirm-an-order
        new AlertDialog.Builder(MainScreen.this)
            .setTitle("Confirmation")
            .setMessage("Are you sure you want to delete " + mainChar.getName() + "?")
            .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getApplicationContext(), "Deleted character " + mainChar.getName(), Toast.LENGTH_SHORT).show();
                    SaveManager.deleteCharacter(position.intValue());
                    SaveManager.saveCharacters(getApplicationContext());
                    Intent y = new Intent(MainScreen.this, TitleScreen.class);
                    startActivity(y);
                }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
                }).setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
    }

    //to be called when changes occur
    private void saveCharacterChanges(){
        SaveManager.replaceCharacter(mainChar, mainChar);
        SaveManager.saveCharacters(getApplicationContext());
    }


    private void initLifeAdjust(){
        adjustLife=findViewById(R.id.editLife);
        damage=findViewById(R.id.getDamage);
        healing=findViewById(R.id.getHealing);
        life=findViewById(R.id.displayLife);
        life.setText("Life: "+mainChar.getHp() + " / " + mainChar.getMaxHp());
        life.setTypeface(null, Typeface.BOLD);


        damage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpsmall.start();
                if(!adjustLife.getText().toString().equals("")){
                    mainChar.setHp(-1*Integer.parseInt(adjustLife.getText().toString()));
                    life.setText("Life: "+mainChar.getHp() + " / " + mainChar.getMaxHp());
                    saveCharacterChanges();
                }
                else{
                    Toast.makeText(getApplicationContext(), "No damage received. Gratz!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        healing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpsmall.start();
                if(!adjustLife.getText().toString().equals("")){
                    mainChar.setHp(Integer.parseInt(adjustLife.getText().toString()));
                    life.setText("Life: "+mainChar.getHp() + " / " + mainChar.getMaxHp());
                    saveCharacterChanges();
                }
                else{
                    Toast.makeText(getApplicationContext(), "You got 0 healing. RIP", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    //drawer related
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }


        return super.onOptionsItemSelected(item);
    }


    private void initStats(){
        TextView str=findViewById(R.id.displayStr);
        TextView dex=findViewById(R.id.displayDex);
        TextView con=findViewById(R.id.diplayCon);
        TextView intel=findViewById(R.id.displayInt);
        TextView wis = findViewById(R.id.displayWis);
        TextView charisma=findViewById(R.id.displayChar);

        str.setText("Strength: "+mainChar.getStrength());
        str.setTypeface(null, Typeface.BOLD);
        dex.setText("Dexterity: "+mainChar.getDexterity());
        dex.setTypeface(null, Typeface.BOLD);
        con.setText("Constitution: "+mainChar.getConstitution());
        con.setTypeface(null, Typeface.BOLD);
        intel.setText("Intelligence: "+mainChar.getIntelligence());
        intel.setTypeface(null, Typeface.BOLD);
        wis.setText("Wisdom: "+mainChar.getWisdom());
        wis.setTypeface(null, Typeface.BOLD);
        charisma.setText("Charisma: "+mainChar.getCharisma());
        charisma.setTypeface(null, Typeface.BOLD);


    }

    private void initMediaPlayer(){
        mpsmall=MediaPlayer.create(this, R.raw.smallclick);
        mpmid=MediaPlayer.create(this, R.raw.midclick);
        mpbig=MediaPlayer.create(this, R.raw.bigclick);
    }

    private void setCharacter()
    {
        Log.d("TEST", "setting character");
        Intent i = getIntent();
        mainChar=(Character)i.getSerializableExtra("mainChar");
        position=(Integer)i.getSerializableExtra("charIndex");
    }

    //generate dummy content for roll list
    private void generateListContent(){
        for(int i=0;i<mainChar.getRolls().size();i++){
            data.add("This is row number "+i);
        }
    }

    //Custom ListView related
    private class MyListAdapter extends ArrayAdapter<String> implements Serializable{
        private int layout;
        public MyListAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
            layout=resource;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final int pos = position;

            LayoutInflater inflater= LayoutInflater.from(getContext());
            convertView=inflater.inflate(layout, parent, false);

            //use ids from roll list item
            final ViewHolder viewHolder = new ViewHolder();
            final Throw toUse = mainChar.getRolls().get(position);
            viewHolder.name=convertView.findViewById(R.id.roll_list_name);
            if(toUse.getBonus()==0){
                viewHolder.name.setText(toUse.getName()+" : "+toUse.getNumbers()+"D"+toUse.getFaces());
            }else{
                viewHolder.name.setText(toUse.getName()+" : "+toUse.getNumbers()+"D"+toUse.getFaces()+"+"+toUse.getBonus());
            }


            viewHolder.result=convertView.findViewById(R.id.roll_list_result);
            viewHolder.result.setText("");
            viewHolder.roll=convertView.findViewById(R.id.roll_list_launch);
            viewHolder.roll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mpsmall.start();
                    toUse.launch();
                    int total = toUse.getTotal();
                    if(toUse.getNumbers()==1 && toUse.getBonus()==0){
                        if(toUse.getBonus()==0 && toUse.getFaces()==20 && total==20){
                            viewHolder.result.setText("You rolled a " + total +".Critical Hit!");
                        }else{
                            viewHolder.result.setText("You rolled a " + total +".");
                        }
                    }else{
                        viewHolder.result.setText("You rolled a " + total + " with  " + toUse.getAllThrows());
                    }

                }
            });

            viewHolder.edit=convertView.findViewById(R.id.roll_list_edit);
            viewHolder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mpmid.start();
                    //Class a= null;

                        Throw toEdit=mainChar.getRolls().get(pos);
                        Intent x = new Intent(MainScreen.this, CreateEditThrowActivity.class);
                        x.putExtra("toEdit", toEdit).putExtra("mainChar", mainChar);
                        startActivity(x);


                }
            });

            convertView.setTag(viewHolder);

            return convertView;
        }
    }

    public class ViewHolder {
        TextView name;
        TextView result;
        Button roll;
        Button edit;
    }
}
