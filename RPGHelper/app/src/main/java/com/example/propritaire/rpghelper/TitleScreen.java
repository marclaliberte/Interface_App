package com.example.propritaire.rpghelper;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.propritaire.rpghelper.assets.Character;
import com.example.propritaire.rpghelper.assets.RandomName;
import com.example.propritaire.rpghelper.assets.SaveManager;

import java.util.ArrayList;

public class TitleScreen extends AppCompatActivity
{
    Button createNewChar;
    ListView charList;

    //sounds related
    MediaPlayer mpsmall;
    MediaPlayer mpmid;
    MediaPlayer mpbig;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_screen);
        //Change top title
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#FFFFFF'>RPGHelper - Load a Character</font>"));
        //change nav bar colors
        getWindow().setNavigationBarColor(getColor(R.color.black));

        SaveManager.initialize();
        SaveManager.loadCharacters(this);

        initCreateButton();

        charList = findViewById(R.id.charList);
        initCharList();

        initMediaPlayer();
    }

    private void initMediaPlayer(){
        mpsmall=MediaPlayer.create(this, R.raw.smallclick);
        mpmid=MediaPlayer.create(this, R.raw.midclick);
        mpbig=MediaPlayer.create(this, R.raw.bigclick);
    }


    private void initCharList(){
        //Create list of character descriptions
        ArrayList<String> charDescs = new ArrayList<>();
        if(SaveManager.characters != null) {
            for (int i = 0; i < SaveManager.characters.size(); i++) {
                charDescs.add(SaveManager.characters.get(i).getCharDesc());
            }
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(TitleScreen.this, android.R.layout.simple_list_item_1 , charDescs);
        charList.setAdapter(arrayAdapter);
        charList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mpmid.start();
                Intent i =new Intent(TitleScreen.this, MainScreen.class);
                i.putExtra("mainChar", SaveManager.getCharacter(position));
                i.putExtra("charIndex", position);
                mpmid.start();
                startActivity(i);
            }
        });
    }


    private void initCreateButton() {
        createNewChar = findViewById(R.id.charCreate);
        createNewChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpmid.start();
                startActivity(new Intent(TitleScreen.this, CharacterActivity.class));
            }
        });
    }


}
