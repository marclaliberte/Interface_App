package com.example.propritaire.rpghelper.assets;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.propritaire.rpghelper.TitleScreen;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;


public class SaveManager
{
    public static ArrayList<Character> characters;

    //Save la liste de la classe
    public static void saveCharacters(Context c)
    {
        try {
            // write object to file
            String location = c.getFilesDir().getAbsolutePath() + "characters.dat";
            FileOutputStream fos = new FileOutputStream(location);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(characters);
            oos.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TEST", "erreur save : " + e.getMessage());
        }
    }

    public static ArrayList<Character> loadCharacters(Context c)
    {
        try{
            // read object from file
            String location = c.getFilesDir().getAbsolutePath() + "characters.dat";
            FileInputStream fis = new FileInputStream(location);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Character> result = (ArrayList<Character>) ois.readObject();
            ois.close();
            characters = result;
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TEST", "erreur load : " + e.getMessage());
        }
        return null;
    }

    public static void addCharacter(Character c)
    {
        if(characters == null)
        {
            characters = new ArrayList<Character>();
        }

        characters.add(c);
    }


    public static void replaceCharacter(Character originalChar, Character newChar){
        for(int i=0;i<characters.size();i++){
            if(characters.get(i).equals(originalChar)){
                Log.d("TEST", "replacing character");
                characters.set(i,newChar);
            }
        }
    }

    public static Character getCharacter(int index)
    {
        return characters.get(index);
    }

    public static void initialize()
    {
        if(characters == null)
        {
            characters = new ArrayList<Character>();
        }
    }

    public static void deleteCharacter(int index)
    {
        characters.remove(index);
    }

    public static void deleteCharacter(Character c)
    {
        characters.remove(c);
    }

    public static int getIndex(Character c)
    {
        return characters.contains(c) ? characters.indexOf(c) : -1;
    }


}
