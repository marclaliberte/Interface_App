package com.example.propritaire.rpghelper;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.propritaire.rpghelper.assets.Character;
import com.example.propritaire.rpghelper.assets.SaveManager;
import com.example.propritaire.rpghelper.assets.Throw;

public class CreateEditThrowActivity extends AppCompatActivity {
    Button save;
    Button delete;
    EditText name;
    EditText number;
    EditText face;
    EditText bonus;
    Character mainChar;
    Throw toEdit;
    MediaPlayer mpmid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // Log.d("TEST", "dans create throw");

        //change nav bar color
        getWindow().setNavigationBarColor(getColor(R.color.black));
        //change title
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#FFFFFF'>RPGHelper - Create/Edit a Throw</font>"));


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_throw);

        save=findViewById(R.id.saveThrowBtn);
        delete=findViewById(R.id.throwDeleteBtn);
        delete.setVisibility(View.INVISIBLE);

        Intent j = getIntent();

        mainChar=(Character)j.getSerializableExtra("mainChar");
        toEdit=(Throw)j.getSerializableExtra("toEdit");
        //Log.d("TEST", toEdit.getFaces()+"");

        mpmid=MediaPlayer.create(this, R.raw.midclick);

        number = findViewById(R.id.throwNumber);
        face = findViewById(R.id.throwFaces);
        name = findViewById(R.id.throwName);
        bonus = findViewById(R.id.throwBonus);

        //delete button will be hidden when create is launched,
        //visible when edit is launched.
        if(toEdit!=null){
            initThrowToEdit();
            delete.setVisibility(View.VISIBLE);

        }
        initButtons();
    }

    private void initButtons(){
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Throw newRoll = createThrow();
                mpmid.start();
                if(newRoll != null && !mainChar.getRolls().contains(newRoll))
                {
                    Toast.makeText(getApplicationContext(), "Saving throw named " + newRoll.getName(), Toast.LENGTH_LONG).show();
                    if(toEdit==null){
                        mainChar.getRolls().add(newRoll);
                    }else{
                        int index=mainChar.getThrowIndex(toEdit.getName());
                        mainChar.getRolls().set(index,newRoll);
                    }

                    SaveManager.replaceCharacter(mainChar, mainChar);
                    SaveManager.saveCharacters(getApplicationContext());
                    Intent i = new Intent(CreateEditThrowActivity.this, MainScreen.class);
                    i.putExtra("mainChar", mainChar);
                    mpmid.start();
                    startActivity(i);
                }
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpmid.start();
                //Throw newRoll = createThrow();
                mainChar.enleverRoll(toEdit.getName());
                SaveManager.replaceCharacter(mainChar, mainChar);
                SaveManager.saveCharacters(getApplicationContext());
                Intent i = new Intent(CreateEditThrowActivity.this, MainScreen.class);
                i.putExtra("mainChar", mainChar);
                mpmid.start();
                startActivity(i);
            }
        });
    }

    private Throw createThrow()
    {
        String na = name.getText().toString();
        String numText = number.getText().toString();
        String fText = face.getText().toString();
        String bText = bonus.getText().toString();


        if(na.equals("") || numText.equals("") || fText.equals("")) {
            Toast.makeText(getApplicationContext(), "Invalid roll", Toast.LENGTH_LONG).show();
            return null;
        }

        try {
            int b = bText.equals("") ? 0 : Integer.parseInt(bText);
            int nu = Integer.parseInt(numText);
            int f = Integer.parseInt(fText);

            return new Throw(na, nu, f, b);
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(), "Invalid roll", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    private void initThrowToEdit(){
        name.setText(toEdit.getName());
        face.setText(toEdit.getFaces()+"");
        number.setText(toEdit.getNumbers()+"");
        bonus.setText(toEdit.getBonus()+"");
    }

}


