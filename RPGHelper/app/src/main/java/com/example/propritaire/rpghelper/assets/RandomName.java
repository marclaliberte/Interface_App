package com.example.propritaire.rpghelper.assets;


public class RandomName {
    private String[] vowels={"a","e","i","o","u", "y", "ie","ae","ui","ion"};

    private String[] cons={"d", "g","k", "m","p","x", "th", "r", "kh", "gh", "ro", "rd", "uk","ok",
                            "il","kan", "gn", "md","gr","hel","gon","wen",
                            "hil","mn","nor","rod","gw","thr","dha","ech","oth","abd","rk","tau","iel","ius"};

    private String[] title={"Destroyer", "Barbarian", "Just", "Evil", "Eater of Worlds", "Paragon",
                            "Twisted", "Spiked", "Explorer", "Invisible", "Quick", "Deadly", "Kingslayer",
                            "Dead", "Hunter", "Protector","Nimble", "Savage", "Firelord", "Knight",
                        	"Battle Lord", "Challenged", "Broken", "Dark Master", "HighBorn", "LowBorn",
                        	"Gladiator", "Bastard", "Gorgeous", "Gullible", "Slayer", "Undying", "Vanquisher",
                        	"Betrayer", "Relentless","Breaker of Chains", "Mad King", "Butcher", "Sinful"};//lol

    private String[][] alpha={vowels,cons};

    private String capitalizeFirstLetter(String input)
    {
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    public String generateName()
    {
        double x=Math.round(Math.random());
        String result="";
        //le 5 ici est un choix random qui me semblait assez
        for(int i=0;i<5;i++){
            result+=alpha[(int)x%2][(int)Math.floor(Math.random()*alpha[(int)x%2].length)];
            //si le mot a 3 choix ou plus, 1 chances sur 3 detre assez
            if(i>=2){
                if(Math.random()<(1/3)){
                    break;
                }
            }
            x++;
        }
        return capitalizeFirstLetter(result);
    }

    public String generateNameWithTitle()
    {
        return generateName()+" the " +title[(int)Math.floor(Math.random()*title.length)];
    }
}
