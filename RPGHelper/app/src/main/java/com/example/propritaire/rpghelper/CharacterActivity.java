package com.example.propritaire.rpghelper;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.propritaire.rpghelper.assets.Character;
import com.example.propritaire.rpghelper.assets.RandomName;
import com.example.propritaire.rpghelper.assets.SaveManager;
import com.example.propritaire.rpghelper.assets.Throw;

public class CharacterActivity extends AppCompatActivity{
   //name related
    Button nameGen;
    Button nameGenTitle;
    RandomName rand;

    //stats roller
    Button strRoller;
    Button dexRoller;
    Button conRoller;
    Button intRoller;
    Button wisRoller;
    Button charRoller;

    //MediaPlayer
    MediaPlayer mpsmall;
    MediaPlayer mpmid;
    MediaPlayer mpbig;
    MediaPlayer isengard;

    EditText charName;
    EditText charClass;
    EditText charHP;
    EditText charRace;
    EditText charXp;
    EditText charLvl;

    EditText charStr;
    EditText charDex;
    EditText charCon;
    EditText charInt;
    EditText charWis;
    EditText charCha;
    //alignement
    private Spinner spinner;

    Button saveBtn;

    Character charac;

    //True if we are in edit mode
    boolean editMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setNavigationBarColor(getColor(R.color.black));

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#FFFFFF'>Create/Edit a Character</font>"));

        setContentView(R.layout.activity_character);
        initRandomName();
        initStatRoller();
        initAlignment();
        initSaveChar();
        initEditTexts();
        initMediaPlayer();
        checkEditMode();
    }

    //Checks if we're editing a character
    private void checkEditMode()
    {
        Intent i = getIntent();
        if(i.hasExtra("mainChar"))
        {
            editMode = true;
            charac=(Character)i.getSerializableExtra("mainChar");

            charName.setText(charac.getName());
            charClass.setText(charac.getChar_class());
            charHP.setText(charac.getMaxHp()+"");
            charRace.setText(charac.getRace());
            for(int j=0;j<paths.length; j++)
            {
                if(paths[j].equals(charac.getAlignment()))
                {
                    spinner.setSelection(j);
                    break;
                }
            }
            charXp.setText(charac.getXp_pts()+"");
            charLvl.setText(charac.getLevel()+"");

            charStr.setText(charac.getStrength()+"");
            charDex.setText(charac.getDexterity()+"");
            charCon.setText(charac.getConstitution()+"");
            charInt.setText(charac.getIntelligence()+"");
            charWis.setText(charac.getWisdom()+"");
            charCha.setText(charac.getCharisma()+"");
        }
        else{
            editMode = false;
        }
    }

    @Override
    public void onBackPressed() {
        Intent mainIntent = new Intent(this, TitleScreen.class);
        startActivityForResult(mainIntent, 0);
    }

    private void initEditTexts()
    {
        charName = findViewById(R.id.charName);
        charClass = findViewById(R.id.charClass);
        charHP = findViewById(R.id.charHP);
        charRace = findViewById(R.id.charRace);
        charXp = findViewById(R.id.charXp);
        charLvl = findViewById(R.id.charLvl);

        charStr = findViewById(R.id.charStr);
        charDex = findViewById(R.id.charDex);
        charCon = findViewById(R.id.charCon);
        charInt = findViewById(R.id.charInt);
        charWis = findViewById(R.id.charWis);
        charCha = findViewById(R.id.charCha);
    }

    private void initMediaPlayer(){
        mpsmall= MediaPlayer.create(this, R.raw.smallclick);
        mpmid=MediaPlayer.create(this, R.raw.midclick);
        mpbig=MediaPlayer.create(this, R.raw.bigclick);
        isengard=MediaPlayer.create(this, R.raw.isengard);
    }
    private void initSaveChar()
    {
        saveBtn = findViewById(R.id.saveCharBtn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Character newChar = createCharacter();
                mpbig.start();
                if(newChar != null)
                {
                    Intent inte = getIntent();
                    if(inte.hasExtra("mainChar"))
                    {
                        SaveManager.replaceCharacter(charac, newChar);
                        SaveManager.saveCharacters(getApplicationContext());
                    }
                    else
                    {
                        SaveManager.addCharacter(newChar);
                        SaveManager.saveCharacters(getApplicationContext());
                    }
                    Intent i = new Intent(CharacterActivity.this, TitleScreen.class);
                    startActivity(i);
                }
            }
        });
    }

    private Character createCharacter()
    {
        String nameS = charName.getText().toString();
        String classS = charClass.getText().toString();
        String hpS = charHP.getText().toString();
        String raceS = charRace.getText().toString();
        String aliS = spinner.getSelectedItem().toString();
        String xpS = charXp.getText().toString();
        String lvlS = charLvl.getText().toString();

        String strS = charStr.getText().toString();
        String dexS = charDex.getText().toString();
        String conS = charCon.getText().toString();
        String intS = charInt.getText().toString();
        String wisS = charWis.getText().toString();
        String chaS = charCha.getText().toString();

        String[] values = {nameS, classS, hpS, raceS, aliS, xpS, lvlS, strS, dexS, conS, intS, wisS, chaS};
        if(!checkIfEmpty(values))
        {
            try {
                int hp = Integer.parseInt(hpS);
                int xp = Integer.parseInt(xpS);
                int lvl = Integer.parseInt(lvlS);

                int str = Integer.parseInt(strS);
                int dex = Integer.parseInt(dexS);
                int con = Integer.parseInt(conS);
                int inte = Integer.parseInt(intS);
                int wis = Integer.parseInt(wisS);
                int cha = Integer.parseInt(chaS);
                Character c;
                if(editMode) {
                    c = new Character(nameS, raceS, classS, aliS, str, dex, con, inte, wis, cha, charac.getHp(), lvl, xp, hp, charac.getRollsList());
                }
                else {
                    c = new Character(nameS, raceS, classS, aliS, str, dex, con, inte, wis, cha, hp, lvl, xp);
                }
                return c;
            }
            catch (Exception e){
                Toast.makeText(getApplicationContext(), "Some information are incorrect", Toast.LENGTH_LONG).show();
                return null;
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Some information are incorrect", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    //returns if an array contains an empty value
    private boolean checkIfEmpty(String[] a)
    {
        if(a != null) {
            for (String s : a) {
                if (s.equals("")) {
                    return true;
                }
            }
        }
        return false;
    }

    private void initRandomName()
    {
        //random Name related
        rand = new RandomName();
        nameGen = findViewById(R.id.nameGenBtn);
        nameGenTitle = findViewById(R.id.nameGenTitleBtn);
        nameGen.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mpsmall.start();
                String newName=rand.generateName();
                EditText nameToSet=findViewById(R.id.charName);
                nameToSet.setText(newName);
                nameToSet.clearFocus();
            }
        });
        nameGenTitle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mpsmall.start();
                String newName=rand.generateNameWithTitle();
                EditText nameToSet=findViewById(R.id.charName);
                nameToSet.setText(newName);
                nameToSet.clearFocus();
            }
        });
    }

    public void initStatRoller()
    {
        strRoller=findViewById(R.id.rollStr);
        dexRoller=findViewById(R.id.rollDex);
        conRoller=findViewById(R.id.rollCon);
        intRoller=findViewById(R.id.rollInt);
        wisRoller=findViewById(R.id.rollWis);
        charRoller=findViewById(R.id.rollChar);

        //strength
        strRoller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpsmall.start();
                Throw a= new Throw("4D6",4,6,0);
                a.launch();
                EditText toSet=findViewById(R.id.charStr);
                TextView textToSet=findViewById(R.id.strResult);
                toSet.setText(a.getTotal()+"");
                textToSet.setText(a.getAllThrows());
            }
        });
        //dexterity
        dexRoller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpsmall.start();
                Throw a= new Throw("4D6",4,6,0);
                a.launch();
                EditText toSet=findViewById(R.id.charDex);
                TextView textToSet=findViewById(R.id.dexResult);
                toSet.setText(a.getTotal()+"");
                textToSet.setText(a.getAllThrows());
            }
        });
        //constitution
        conRoller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpsmall.start();
                Throw a= new Throw("4D6",4,6,0);
                a.launch();
                EditText toSet=findViewById(R.id.charCon);
                TextView textToSet=findViewById(R.id.conResult);
                toSet.setText(a.getTotal()+"");
                textToSet.setText(a.getAllThrows());
            }
        });
        //intelligence
        intRoller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpsmall.start();
                Throw a= new Throw("4D6",4,6,0);
                a.launch();
                EditText toSet=findViewById(R.id.charInt);
                TextView textToSet=findViewById(R.id.intResult);
                toSet.setText(a.getTotal()+"");
                textToSet.setText(a.getAllThrows());
            }
        });

        //wisdom
        wisRoller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpsmall.start();
                Throw a= new Throw("4D6",4,6,0);
                a.launch();
                EditText toSet=findViewById(R.id.charWis);
                TextView textToSet=findViewById(R.id.wisResult);
                toSet.setText(a.getTotal()+"");
                textToSet.setText(a.getAllThrows());
            }
        });
        //charisma
        charRoller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpsmall.start();
                Throw a= new Throw("4D6",4,6,0);
                a.launch();
                EditText toSet=findViewById(R.id.charCha);//charchar lol
                TextView textToSet=findViewById(R.id.charResult);
                toSet.setText(a.getTotal()+"");
                textToSet.setText(a.getAllThrows());
            }
        });

    }
    private static final String[]paths = {"Lawfull Good", "Lawfull Neutral", "Lawfull Good", "Neutral Good", "True Neutral","Neutral Evil", "Chaotic Good", "Chaotic Neutral", "Chaotic Evil"};
    /*https://stackoverflow.com/questions/13377361/how-to-create-a-drop-down-list*/
    public void initAlignment(){
        spinner = findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CharacterActivity.this,
                android.R.layout.simple_spinner_item,paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }


    public static class CreateEditThrowActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_create_edit_throw);
        }
    }
}
