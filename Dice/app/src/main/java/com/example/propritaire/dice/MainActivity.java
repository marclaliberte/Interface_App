package com.example.propritaire.dice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    Button roll;
    EditText number;
    EditText faces;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        roll =(Button) findViewById(R.id.roll);
        number = findViewById(R.id.number);
        faces = findViewById(R.id.face);
        result = findViewById(R.id.result);

        roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String face = faces.getText().toString();
                String num = number.getText().toString();
                String name = face+"d"+num;

                int f = Integer.parseInt(face);
                int n = Integer.parseInt(num);
                Throw t = new Throw(name, n, f);

                int r = t.launch();
                result.setText(String.valueOf(r));
            }
        });
    }

}
